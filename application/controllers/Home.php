<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    // halaman utama aplikasi (homepage)
    public function index()
    {
        $data = array(  'title'     => 'LangensariSteell',
                        'isi'       => 'home/list'
                     );
        $this->load->view('layout/wrapper', $data, FALSE);               
    }

}

/* End of file Home.php */
