<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Simple_login
{
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        // Load data model user
        $this->CI->load->model('user_model');
    }

    //fungsi login
    public function login($username, $password)
    {
        $check = $this->CI->user_model->login($username,$password);
        //jika ada data user maka ada create session
        if($check)
        {
            $id_user    = $check->id_user;
            $nama       = $check->nama;      
            $role       = $check->role;
            // Create session
            $this->CI->session->set_userdata('id_user', $id_user);
            $this->CI->session->set_userdata('nama', $nama);
            $this->CI->session->set_userdata('username', $username);
            $this->CI->session->set_userdata('role', $role);
            //redirect ke halaman admin yang diproteksi
            redirect(base_url('admin/dasbor'),'refresh');
            
        } else {
            //jika tidak ada data, maka harus login kembali
            $this->CI->session->set_flashdata('warning', 'Username atau password salah');
            redirect(base_url('login'),'refresh'); 
        }
    }

    //fungsi cek login
    public function cek_login()
    {
        //Memeriksa pakah session-nya sudah ada atau belum, jika belum alihkan ke halaman login
        if($this->CI->session->userdata('username') == "") 
        {
            $this->CI->session->set_flashdata('warning', 'Anda belum login');
            redirect(base_url('login'),'refresh');
            
        }
    }

    //fungsi logout
    public function logout()
    {
        //membuang semua session yang telah diset pada saat login
        $this->CI->session->unset_userdata('id_user');
        $this->CI->session->unset_userdata('nama');
        $this->CI->session->unset_userdata('username');
        $this->CI->session->unset_userdata('role');
        //setelah session dibuang, maka redirect ke login
        $this->CI->session->set_flashdata('sukses', 'Anda berhasil logout');                                                                     
        redirect(base_url('login'),'refresh');
        
    }

    

}

/* End of file Simple_login.php */
